// [SECTION] Dependencies and Modules
	const express = require('express');	
	const mongoose = require('mongoose');
	// const dotenv = require('dotenv');
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders')
	const cartRoutes = require('./routes/carts')
	const checkoutRoutes = require('./routes/checkouts')
	const contactRoutes = require('./routes/contact')
	const cors = require('cors');

	

// [SECTION] Environment
	// dotenv.config();
	// let dbConnect = process.env.DBCREDENTIALS;
	const port = 4000;

// [SECTION] Server
	const app = express();
	
	app.use(cors());
	app.use(express.json());
    app.use(express.urlencoded({extended: true}));


	// const conStatus = mongoose.connection;
	// conStatus.once('open', () => console.log('Database Connection Established'));

// [SECTION] Backend Routes
	app.use('/uploads',express.static('uploads'));
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes);
	app.use('/carts', cartRoutes);
	app.use('/checkouts', checkoutRoutes);
	app.use('/contact', contactRoutes);


// [SECTION] Database Conenction
	mongoose.connect("mongodb+srv://carlosmiguelyulo813:carlomigS813420@zuitt-bootcamp.4nsvfgb.mongodb.net/Capstone-3?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
// [SECTION] Gateway Response
	let db = mongoose.connection

	db.on('error', () => console.log.bind(console, 'error'));
	db.once('open', () => console.log('Now connected to MongoDB Atlas'));

	app.get('/', (req, res) => {
		res.send('Landing Page')
	});

	app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
	});