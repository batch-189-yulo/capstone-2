// [SECTION] Dependencies and Modules
	const jwt = require('jsonwebtoken');
	const secret = 'EcommerceAPI';

// [SECTION] Token Creation
	module.exports.createAccessToken = (user) => {
		const data = {
			id: user._id,
			firstName: user.firstName,
			lastName: user.lastName,
			email: user.email,
			isAdmin: user.isAdmin
		};

		return jwt.sign(data, secret, {});
	};

// [SECTION] Token Verification
	module.exports.verify = (req, res, next) => {
		let token = req.headers.authorization;

		if (typeof token === "undefined") {
			return res.send({auth: "failed. No token"});
		} else {
			token = token.slice(7, token.length);

			jwt.verify(token, secret, function(error, tokenDecode) {
				if (tokenDecode) {
					req.user = tokenDecode;
					next();
				} else {
					return res.send({
						auth: "Failed",
						message: error.message
					});
				};
			});
		};
	};

// [SECTION] Admin Verification and middleware
	module.exports.verifyAdmin = (req, res, next) => {
		let admin = req.user.isAdmin;
		if (admin) {
			next();
		} else {
			return res.send({
				auth: "Failed- Not Admin",
				message: "Not Authorized"
			});
		};

	};