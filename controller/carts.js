// [SECTION] Dependencies and modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Cart = require('../models/Cart');
	const auth = require('../auth');

// [SECTION] Functionalities [CREATE]
	// [SUBSECTION] Adding Products to Cart by Authenticated User
	module.exports.purchase = async (req, res) => {
		let adminCheck = req.user.isAdmin;
		let userId = req.user.id;
		let productId = req.body.productId;

		if (adminCheck) {
			return res.send({ message: "action not allowed"})
		};

		let productOrder = Product.findById(productId).then(product => {
			let UpdateCart = {
				productId: product.id,
				name: product.productName,
				price: product.price,
				quantity: req.body.quantity,
				subTotal: product.price * req.body.quantity
			};

			// [FEATURES] Creates New Cart Array to User or Appends New Orders to Existing Cart
			let userCart = Cart.findOne({userId: userId}).then( cart =>{
				if (cart) {
					cart.productCart.push(UpdateCart);
					return cart.save().then((user, err) => {
						if (user) {
							return res.send({message:"more items to cart"})
						} else {
							return false;
						};
					});
				} else {
					let newCart = new Cart ({
						userId: userId,
						productCart: [{
						productId: product.id,
						name: product.productName,
						price: product.price,
						quantity: req.body.quantity,
						subTotal: product.price * req.body.quantity
						}],
					});
					return newCart.save().then((user, err) => {
						if (user) {
							return res.send({message: "added to cart"});
						} else {
							return false;
						};
					})
				}
			});
		});
	};

// [SECTION] Functionalities [RETRIEVE]
	// [SUBSECTION] Retrieve Authenticated User Orders from Cart (User)
		module.exports.getOrders = (userId) => {
			return Cart.findOne({userId: userId}).then(outcome => {
				if (outcome) {
					return outcome.productCart;
				} else {
					return false;
				}
			});
		};

	// [SUBSECTION] Retrieve Records of ALL Orders (Admin)
		module.exports.getAllOrders = () => {
			return Order.find({}).then(outcome => {
				return outcome;
			})
		}

// [SECTION] Functionalities [UPDATE]
	// [SUBSECTION] Updating Products (Admin)
		module.exports.updateCart = (req,res) => {
			// let updatedCartInfo = cartData.quantity;
			let userId = req.user.id;
			let cartId = req.params.cartId;
			let updatedCartInfo = req.body.quantity;

			let findCart = Cart.findOne(
				{userId: userId,'productCart._id': cartId},{'productCart.$':1}
				).then((cart, error) =>{
					if (cart) {
						let cartInfo = cart.productCart[0];
						let updatedSubtotal = cartInfo.price * updatedCartInfo;
						return Cart.findOneAndUpdate(
							{"productCart._id":cartId},
							{$set: 
								{
									"productCart.$.quantity":updatedCartInfo,
									"productCart.$.subTotal":updatedSubtotal, 
								}}
						).then((cart,error) => {
							if (cart) {
								return res.send (true);
							} else {
								return res.send (false);
							};
						});

					} else {
						return res.send (false);
					}						
				})





		};
// [SECTION] Functionalities [DELETE]
	module.exports.deleteCart = (userId, cartId) => {
		return Cart.updateOne(
			{userId: userId},
			{"$pull": {
				"productCart": {
					"_id": cartId
				}
			}}
		).then((cart,error) => {
			if (cart) {
				return { Delete: "Complete"};
			} else {
				return false;
			};
		})
	};