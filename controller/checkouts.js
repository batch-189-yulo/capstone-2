// [SECTION] Dependencies and modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Cart = require('../models/Cart');
	const Checkout = require('../models/Checkout');  
	const auth = require('../auth');

// [SECTION] Functionalities [CREATE]
	// [SUBSECTION] Checkout Functionalities (Cart is Cleared After Sucessful Checkout)
		module.exports.purchase = (req, res) => {
			let adminCheck = req.user.isAdmin;
			let purchaseCheck = req.body.checking;
			let userId = req.user.id;

			if (adminCheck) {
				return res.send({ message: "action not allowed"})
			};

			if (purchaseCheck === false) {
				return res.send({ message: "continue shopping"});
			};

			let userCart = Cart.findOne({userId: userId}).then (cart => {
				if (cart) {
					let checkoutId = cart.id;
					let productSum = 0;
					for (let indexCount = 0; indexCount < cart.productCart.length; indexCount++) {
							let array = cart.productCart[indexCount];
							productSum += array.subTotal;
					}

					let newCheckout = new Checkout ({
						userId: req.user.id,
						firstName: req.user.firstName,
						lastName: req.user.lastName,
						products: cart.productCart,
						totalAmount: productSum
					});

					return newCheckout.save().then((checkout, err) => {
							if (checkout) {
								Cart.findByIdAndRemove(checkoutId).then((product,error) => {
									if (product) {
										return res.send({message: "Item Checked Out"});
									} else {
										return false;
									};
								}).catch(error => error);

							} else {
								return {message: "error"}
							};
						})

				} else {
					return res.send({message: "shopping cart is empty"});
				};
			});
		};

// [SECTION] Functionalities [RETRIEVE]
	// [SUBSECTION] Retrieve Authenticated User Checkout History (User)
		module.exports.getOrders = (userId) => {
			return Checkout.find({userId: userId}).then(outcome => {
				return outcome;
			});
		};

	// [SUBSECTION] Retrieve Authenticated User Checkout History Products
		module.exports.getOrderProducts = (orderId) => {
			return Checkout.findById({_id: orderId,}).then(outcome => {
				return outcome.products;
			});
		};

	// [SUBSECTION] Retrieve Records of ALL Checkouts (Admin)
		module.exports.getAllOrders = () => {
			return Checkout.find({}).then(outcome => {
				return outcome;
			})
		}

// [SECTION] Functionalities [UPDATE]
// [SECTION] Functionalities [DELETE]