
const Contact = require('../models/Contact')



// [SECTION] Contacting the Web Page [POST]
	module.exports.contactMessage = (contactData) => {
			let firstName = contactData.firstName;
			let lastName = contactData.lastName;
			let email = contactData.email;
			let message = contactData.message;

			let newContact = new Contact({
				firstName: firstName,
				lastName: lastName,
				email: email,
				message: message
			
			});

			return newContact.save().then((contact, err) => {
				if (contact) {
					return contact;
				} else {
					return 'Failed to Send message';
				};
			}).catch(error => error.message);
		};