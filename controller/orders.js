// [SECTION] Dependencies and modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Order = require('../models/Order');  
	const auth = require('../auth');

// [SECTION] Functionalities [CREATE]
	module.exports.purchase = async (req, res) => {
		let adminCheck = req.user.isAdmin;
		let productId = req.body.productId;

		if (adminCheck) {
			return res.send({ message: "action not allowed"})
		};

		let productOrder = Product.findById(productId).then(product => {
			let newOrder = new Order ({
				userId: req.user.id,
				firstName: req.user.firstName,
				lastName: req.user.lastName,
				products: [{
				productId: product.id,
				name: product.productName,
				price: product.price,
				quantity: req.body.quantity,
				}],
				totalAmount: product.price * req.body.quantity
			});
				
			return newOrder.save().then((order,err) => {
				if (order) {
					return res.send({message: "order successfully placed"});
				} else {
					return false;
				};
			}).catch(error => error.message);
		});
	};

// [SECTION] Functionalities [RETRIEVE]
	// [SUBSECTION] Retrieve Authenticated User Orders (User)
		module.exports.getOrders = (userId) => {
			return Order.find({userId: userId}).then(outcome => {
				return outcome;
			});
		};

	// [SUBSECTION] Retrieve Records of ALL Orders (Admin)
		module.exports.getAllOrders = () => {
			return Order.find({}).then(outcome => {
				return outcome;
			})
		}

// [SECTION] Functionalities [UPDATE]
// [SECTION] Functionalities [DELETE]