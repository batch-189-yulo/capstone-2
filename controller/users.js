// [SECTION] Dependencies and modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Order = require('../models/Order');  
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth');

// [SECTION] Environment Variables Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

	//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email: reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false
		}

	})

}


// [SECTION] Functionalities [CREATE]
	// [SUBSECTION] Create new user
		module.exports.register = (userData) => {
			let firstName = userData.firstName;
			let lastName = userData.lastName;
			let email = userData.email;
			let contactNum = userData.contactNo;
			let passWord = userData.password;

			let newUser = new User({
				firstName: firstName,
				lastName: lastName,
				email: email,
				contactNo: contactNum,
				password: bcrypt.hashSync(passWord, salt)
			});

			return newUser.save().then((user, err) => {
				if (user) {
					return user;
				} else {
					return 'Failed to Register account';
				};
			}).catch(error => error.message);
		};

// [SECTION] Functionalities [RETRIEVE]
	// [SUBSECTION] Retrieve Authenticated(logged in) User Details
		module.exports.getUserProfile = (userData)  => {
			return User.findById(userData).then(outcome => {
				outcome.password = '';
				return outcome;
			});
		};

	// [SUBSECTION] View ALL Registered Users (admin)
		module.exports.retrieveAllUsers = () => {
			return User.find({}).then(outcome => {
				return outcome;
			});
		};

	// [SUBSECTION] Promote User to Admin (admin)
		module.exports.promoteAdmin = (profileId) => {
			let adminField = {
				isAdmin: true
			};

			return User.findByIdAndUpdate(profileId, adminField).then((profile, error) => {
				if (profile) {
					return { message:"successfully promoted"};
				} else {
					return false;
				};
			}).catch(error => error.message);
		}
		
// [SECTION] Functionalities [UPDATE]
// [SECTION] Functionalities [DELETE]







// [SECTION] User Authentication
	module.exports.userLogin = (loginData) => {
		return User.findOne({email: loginData.email}).then(outcome => {
			if (outcome) {
				const passwordCheck = bcrypt.compareSync(loginData.password, outcome.password);

				if (passwordCheck) {
					return {accessToken: auth.createAccessToken(outcome.toObject())};
				} else {
					return false;
				};
			} else {
				return false;
			};
		});
	};
