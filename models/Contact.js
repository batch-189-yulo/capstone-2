// [SECTION] Modules and Dependencies
	const mongoose = require('mongoose');

// [SECTION] Schema (Blueprint)
	const contactSchema = new mongoose.Schema({
		firstName:{
			type: String,
			required: [true, 'Please fill up your First Name']
		},
		lastName:{
			type: String,
			required: [true, 'Please fill up your Last Name']
		},
		email:{
			type: String,
			required: [true, 'Please fill up your Email Address']
		},
		message:{
			type: String,
			required: [true, 'Please fill up your Phone Number']
		
		},
	});

// [SECTION] Model
	module.exports = mongoose.model('Contact', contactSchema);