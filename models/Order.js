// [SECTIONS] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTIONS] Schema (Blueprint) 
	const orderSchema = new mongoose.Schema({
		userId: {
			type: String,
			required: [true, 'error']
		},
		firstName: {
			type: String,
			required: [true, 'error']
		},
		lastName: {
			type: String,
			required: [true, 'error']
		},
		products: [
			{
				productId:{
					type: String,
					required: [true, 'error']
				},
				name:{
					type: String,
					required: [true, 'error']
				},
				price:{
					type: Number,
					required: [true, 'error']
				},
				quantity:{
					type: Number,
					required: [true, 'error']
				},
			}
		],
		totalAmount: {
			type: Number,
			required: [true, 'error']
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	});

// [SECTIONS] Model
	module.exports = mongoose.model('Order',orderSchema);