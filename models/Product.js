// [SECTIONS] Dependencies and Modules
	const mongoose = require('mongoose');
	
// [SECTIONS] Schema (Blueprint)
	const productSchema = new mongoose.Schema({
		productName: {
			type: String,
			required: [true, 'Product name is Required']
		},
		description: {
			type: String,
			required: [true, 'Product description is Required']
		},
		price: {
			type: Number,
			required: [true, 'Pricing input is Required']
		},
		image: {
			type: String,
		},
		isActive: {
			type: Boolean,
			default: true
		},isFeature: {
			type: Boolean,
			default: false,
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	});

// [SECTIONS] Model
	module.exports = mongoose.model('Product', productSchema);