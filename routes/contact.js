
// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controller/contact');

// [SECTION] Routing Component
	const route = express.Router();




// [SECTION] Contact - POST
		route.post('/contactMessage', (req, res) => {
			let contactData = req.body;
			controller.contactMessage(contactData).then(outcome => {
				res.send(outcome);
			});
		});



	module.exports = route;