// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controller/users');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Authentication Destructuring 
	const {verify, verifyAdmin} = auth;

// [SECTION] User Email Verify
route.post("/checkEmail", (req, res) => {
	controller.checkEmailExists(req.body).then(outcome => {
		res.send(outcome);
	})
})


// [SECTION] User Authentication Login
	route.post('/login', (req, res) => {
		controller.userLogin(req.body).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] Routes - POST
	// [SUBSECTION] User Registration
		route.post('/register', (req, res) => {
			let userData = req.body;
			controller.register(userData).then(outcome => {
				res.send(outcome);
			});
		});

// [SECTION] Routes - GET
	// [SUBSECTION] View Authenticated (Logged in) User Details
		route.get('/details', auth.verify, (req, res) => {
			controller.getUserProfile(req.user.id).then(outcome => res.send(outcome));
		});

	// [SUBSECTION] View ALL registered Users (Admin)
		route.get('/profiles', verify, verifyAdmin, (req, res) => {
			controller.retrieveAllUsers().then(outcome => res.send(outcome));
		});

// [SECTION] Routes - PUT
	// [SUBSECTION] Promote a user account into admin (Admin)
		route.put('/:profileId/admin', verify, verifyAdmin, (req, res) => {
			let profileId = req.params.profileId;
			controller.promoteAdmin(profileId).then(outcome => res.send(outcome));
		});





// [SECTION] Expose Routing System
	module.exports = route;